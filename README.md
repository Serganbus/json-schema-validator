# JSON schema validator

Образ, позволяющий по схеме свалидировать файл или директорию, содержащую json-файлы

# Валидация файла
`docker run --rm -it -v ~/schema.json:/tmp/schema.json -v ~/file-to-validate.json:/tmp/file-to-validate.json -it serganbus/json-schema-validator:latest ./validator validate /tmp/schema.json /tmp/file-to-validate.json`

# Валидация директории с файлами
`docker run --rm -it -v ~/schema.json:/tmp/schema.json -v ~/files-to-validate:/tmp/files-to-validate -it serganbus/json-schema-validator:latest ./validator validate /tmp/schema.json /tmp/files-to-validate`