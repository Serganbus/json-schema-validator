#!/usr/bin/env php
<?php

require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Serganbus\JsonSchemeValidator\ValidateCommand;

$application = new Application();

$application->add(new ValidateCommand());

$application->run();