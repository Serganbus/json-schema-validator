FROM composer:2.4.4 as build

WORKDIR /tmp

COPY composer.* ./
RUN composer install

FROM php:8.1.12-cli-alpine3.16 as final

WORKDIR /home/validator
COPY --from=build /tmp ./
COPY src ./src
COPY validator.php ./validator
RUN chmod +x validator

CMD ["validator"]