<?php

namespace Serganbus\JsonSchemeValidator;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Opis\JsonSchema\{
    Validator,
    ValidationResult,
    Errors\ErrorFormatter,
};

/**
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class ValidateCommand extends Command
{
    protected static $defaultName = 'validate';

    private Validator $validator;

    public function __construct()
    {
        $this->validator = new Validator();

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Валидация файла или директории в соответствии с json-схемой')
            ->addArgument('schema', InputArgument::REQUIRED, 'Путь до схемы, по которой проверять json-файлы')
            ->addArgument('path', InputArgument::REQUIRED, 'Путь до файла/директории с файлами, которые поддаются проверке через json-схему')
            ->setHelp('Эта команда дает информацию по доступным источникам для загрузки портфеля');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $schema = $input->getArgument('schema');
        $path = $input->getArgument('path');

        try {
            $schemaContent = $this->getFileContent($schema);
        } catch (\Exception $ex) {
            $io->error("Schema is broken. Error: {$ex->getMessage()}");
            return Command::FAILURE;
        }

        $errors = [];

        $filesToValidate = $this->getFilesToValidate($path);
        $io->progressStart(count($filesToValidate));
        foreach ($filesToValidate as $fileToValidate) {
            try {
                $fileContent = $this->getFileContent($fileToValidate, true);
            } catch (\Exception $ex) {
                $errors[$fileToValidate] = ['/' => "File is broken. Error: {$ex->getMessage()}"];
                continue;
            }

            /** @var ValidationResult $result */
            $result = $this->validator->validate($fileContent, $schemaContent);
            if (!$result->isValid()) {
                $formattedErrorsData = (new ErrorFormatter())->format($result->error(), false);
                $fileErrors = [];
                foreach ($formattedErrorsData as $key => $errorMsg) {
                    $fileErrors[$key] = $errorMsg;

                }
                $errors[$fileToValidate] = $fileErrors;
            }

            $io->progressAdvance();
        }
        $io->progressFinish();

        $withoutErrors = count($errors) === 0;
        if ($withoutErrors) {
            $io->success("Validation completed succesfully!");
        } else {
            $errMsgArr = [
                "Validation completed with errors."
            ];

            foreach ($errors as $fileName => $errorArr) {
                $errMsgArr[] = "File {$fileName}:";
                foreach ($errorArr as $key => $msg) {
                    $errMsgArr[] = "{$key} => {$msg}";
                }
            }
            $io->error($errMsgArr);
        }

        return $withoutErrors ? Command::SUCCESS : Command::FAILURE;
    }

    private function getFileContent(string $schemaPath, bool $asJson = false): string|array|\stdClass
    {
        if (mb_substr($schemaPath, 0, 4) === 'http') {
            $realPath = $schemaPath;
        } else {
            $realPath = realpath($schemaPath);

            if (!is_readable($realPath)) {
                throw new \Exception("File is not readable!");
            }
        }

        $content = file_get_contents($realPath);
        if ($content === false) {
            throw new \Exception("Cannot read file!");
        }

        $jsonContent = json_decode($content, null, 512, \JSON_THROW_ON_ERROR);

        return $asJson ? $jsonContent : $content;
    }

    private function getFilesToValidate(string $path): array
    {
        $filesToValidate = [];
        $realPath = realpath($path);
        if (is_dir($realPath)) {
            $filesInDir = scandir($realPath);
            foreach ($filesInDir as $fileInDir) {
                if (!preg_match('/.*\.json$/', $fileInDir)) {
                    continue;
                }

                $filesToValidate[] = $realPath . DIRECTORY_SEPARATOR . $fileInDir;
            }
        } else {
            $filesToValidate[] = $realPath;
        }

        return $filesToValidate;
    }
}
